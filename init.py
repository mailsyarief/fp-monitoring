def replace(file_path, line, first_sign, finish_sign, replace_with):
    with open(file_path, 'r') as file:
        filedata = file.read()

    line -= 1
    lines = filedata.split('\n')
    start = lines[line].find(first_sign) + len(first_sign)
    end = lines[line].rfind(finish_sign)
    print(start,end)
    print(file_path)
    print('\tfrom :\t' + lines[line])
    lines[line] = lines[line].replace(lines[line][start: end], replace_with)
    print('\tto :\t' + lines[line])
    content = '\n'.join(lines)

    with open(file_path, 'w') as file:
        file.write(content)

# replace('mongoDB/filebeat/filebeat.yml', 178, 'hosts: ["', ':9200"]', '127.0.0.1')

name = input("who are you (meidy, nut, sabil) ?")

if name == "meidy":
    ip_sabil = input("sabil's ip : ")
    ip_mail = input("mail's ip : ")
    ip_nut = input("nut's ip : ")
    replace("elastic/apm-server/apm-server.yml", 534, '["', ':9200', ip_mail)
    replace("nodeJS/filebeat/filebeat.yml", 178, '["', ':9200', ip_mail)
    replace("nodeJS/metricbeat/metricbeat.yml", 94, '["', ':9200', ip_mail)
    replace("nodeJS/models/db.js", 3, '//', ':27017', ip_sabil)
    replace("nodeJS/webhook.js", 6, "= '", ':3004', ip_nut)
if name == 'sabil' :
    ip_mail = input("mail's ip : ")
    ip_nut = input("nut's ip : ")
    replace('mongoDB/filebeat/filebeat.yml', 178, 'hosts: ["', ':9200"]', ip_mail)
    replace('mongoDB/metricbeat/metricbeat.yml', 94, 'hosts: ["', ':9200"]', ip_mail)
    replace("mongoDB/mongodb.js", 6, "= '", ':3004', ip_nut)
if name == 'nut':
    ip_mail = input("mail's ip : ")
    ip_meidy = input("meidy's ip: ")
    replace('alerting/.env', 1, '//', ':9200', ip_mail)
    replace('spamRequest/insert.js', 4, "= '", "'", ip_meidy)
    replace('spamRequest/checkdowntime.js', 7, 'http://', ':8080', ip_meidy)
if name == 'bayok':
    ip_meidy = input("meidy's ip : ")
    ip_sabil = input("sabil's ip : ")
    replace('slack-bot/.env', 3, '//', ':3003', ip_meidy)
    replace('slack-bot/.env', 4, '//', ':3005', ip_sabil)