import os
from slack_bolt import App
from dotenv import load_dotenv
import requests

load_dotenv()
DEFAULT_CHAT_NODE_SERVER = '''This is available command for node server: 
- `automate node_server [upscale/downscale] [1-10]` to scale node server
- `automate node_server update` to update node server'''
DEFAULT_CHAT_MONGO_SERVER = '''This is available command for mongo server:
- `automate mongodb_server restart` to restart mongodb server'''
DEFAULT_CHAT = "Use `help` to see available Command"
COMMAND_UNAVAILABLE = "Command Unavailable"
HELP = '''Use this command to access node server/mongodb server
- `automate node_server [upscale/downscale] [1-10]` to scale node server
- `automate node_server [update]` to update node server
- `automate mongodb_server [restart]` to restart mongodb server'''
NODE_IP = os.environ.get("NODE_IP")
MONGO_IP = os.environ.get("MONGO_IP")
WEBHOOK_URL = os.environ.get("WEBHOOK_URL")
ALERT_URL = os.environ.get("ALERT_URL")
RED = "ff0000"
GREEN = "00ff00"
print(NODE_IP, MONGO_IP)

# Initializes your app with your bot token and signing secret
app = App(
    token=os.environ.get("SLACK_BOT_TOKEN"),
    signing_secret=os.environ.get("SLACK_SIGNING_SECRET")
)

def send_message(color, author_name, title, text):
    attch = [{
        "color": color,
        "pretext": "--------------",
        "author_name": author_name,
        "author_icon": "http://flickr.com/icons/bobby.jpg",
        "title": title,
        "title_link": "https://api.slack.com/",
        "text": text,
        "image_url": "http://my-website.com/path/to/image.jpg",
        "thumb_url": "http://example.com/path/to/thumb.png",
        "footer": "Kelompok Klasic Mantap!",
        "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
    }]
    headers = {'Content-type': 'application/json'}
    r = requests.post(WEBHOOK_URL, headers=headers, json={"attachments": attch},)
    print("send message to {} with response {}, and status code {}".format(WEBHOOK_URL, r.text, r.status_code))

def alert_fail(action, r):
    send_message(RED, "NodeJS {}".format(action.capitalize()), "Alert {}".format(action.capitalize()), "{} fail with response code {}\nerror: {}".format(action.capitalize(), r.status_code, r.text))

def update_node_server():
    url = "{}/webhook".format(NODE_IP)
    r = requests.get(url)
    print("hit webhook {}, with status code {}".format(url, r.status_code))
    if r.status_code != 200:
        alert_fail("update", r)

def command_unavailable(message, default):
    send_message(RED, COMMAND_UNAVAILABLE, "", "Command `{}` is not available\n{}".format(message, default))

def scale_node_server(action, count):
    url = "{}/webhook/{}/{}".format(NODE_IP, action, count)
    r = requests.get(url)
    print("hit webhook {}, with status code {}".format(url, r.status_code))
    if r.status_code != 200:
        alert_fail(action, r)

def action_mongo_server(action):
    url = "{}/{}".format(MONGO_IP, action)
    r = requests.get(url)
    print("hit webhook {}, with status code {}".format(url, r.status_code))
    if r.status_code != 200:
        alert_fail(action, r)

@app.message("automate")
def automate(message, say):
    words = message['text'].split(' ')
    print("recieve new automation message \"{}\"".format(message['text']))

    if len(words) < 2 or words[0] != 'automate':
        command_unavailable(message['text'], HELP)
        return
    
    destination = words[1]
    if destination == 'node_server':
        action = words[2]
        if action == 'update':
            if len(words) != 3:
                command_unavailable(message['text'], DEFAULT_CHAT_NODE_SERVER)
                return

            print("action: {} with command ({})".format(action, message['text']))
            update_node_server()
            return
        
        if action == 'upscale' or action == 'downscale':
            if len(words) != 4:
                command_unavailable(message['text'], DEFAULT_CHAT_NODE_SERVER)
                return
            
            count = words[3]
            if not count.isdigit():
                print("error: {} is not digit", count)
                send_message(RED, COMMAND_UNAVAILABLE, "", "Cannot use `{}` as upscale count\n{}".format(count, DEFAULT_CHAT_NODE_SERVER))
                return

            if int(count) < 1 or int(count) > 10:
                command_unavailable(message['text'], DEFAULT_CHAT_NODE_SERVER)
                return

            print("action: {}, count: {} with command({})".format(action, count, message['text']))
            scale_node_server(action, count)
            return
    
        command_unavailable(message['text'], DEFAULT_CHAT_NODE_SERVER)
        return
    
    if destination == 'mongodb_server':
        action = words[2]
        if action == 'restart' and len(words) == 3:
            print("action: {} with command ({})".format(action, message['text']))
            action_mongo_server(action)
            return

        command_unavailable(message['text'], DEFAULT_CHAT_MONGO_SERVER)
        return
    
    command_unavailable(message['text'], HELP)

@app.message("help")
def help(message, say):
    send_message(GREEN, "Command", "Available Command", HELP)

@app.event("message")
def handle_message_events(body, logger):
    return

# Start your app
if __name__ == "__main__":
    app.start(port=int(os.environ.get("PORT", 3000)))