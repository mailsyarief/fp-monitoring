import os
from requests.api import head
from requests.models import Response
# Use the package we installed
from slack_bolt import App
from dotenv import load_dotenv
import requests
import urllib

load_dotenv()
DEFAULT_CHAT_NODE_SERVER = "Use `server_node [upscale/downscale] [1-10]` to scale node server"
DEFAULT_CHAT_MONGO_SERVER = "Use `server_mongo restart` to restart mongodb server"
DEFAULT_CHAT = "Use `help` to see available Command"
COMMAND_UNAVAILABLE = "Command Unavailable"
HELP = '''Use this command to access node server/mongodb server
- `server_node [upscale/downscale] [count]` (to upscale/downscale node server)
- `server_node [update]` (to update node server)
- `server_mongo [restart]` (to restart mongodb server)'''
NODE_IP = os.environ.get("NODE_IP")
MONGO_IP = os.environ.get("MONGO_IP")
WEBHOOK_URL = os.environ.get("WEBHOOK_URL")
ALERT_URL = os.environ.get("ALERT_URL")
RED = "ff0000"
GREEN = "00ff00"
print(NODE_IP, MONGO_IP)

# Initializes your app with your bot token and signing secret
app = App(
    token=os.environ.get("SLACK_BOT_TOKEN"),
    signing_secret=os.environ.get("SLACK_SIGNING_SECRET")
)

def send_message(color, author_name, title, text):
    attch = [{
        "color": color,
        "pretext": "--------------",
        "author_name": author_name,
        "author_icon": "http://flickr.com/icons/bobby.jpg",
        "title": title,
        "title_link": "https://api.slack.com/",
        "text": text,
        "image_url": "http://my-website.com/path/to/image.jpg",
        "thumb_url": "http://example.com/path/to/thumb.png",
        "footer": "Kelompok Klasic Mantap!",
        "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
    }]
    headers = {'Content-type': 'application/json'}
    r = requests.post(WEBHOOK_URL, headers=headers, json={"attachments": attch},)
    print(WEBHOOK_URL, r.text, r.status_code)


# Listens to incoming messages that contain "hello"
@app.message("server_node")
def node_server(message, say):
    words = message['text'].split(' ')
    if len(words) < 2:
        send_message(RED, COMMAND_UNAVAILABLE, "", "Command `{}` is not available\n{}".format(message['text'], DEFAULT_CHAT_NODE_SERVER))
        return

    action = words[1]
    if len(words) == 2 and action == 'update':
        url = "{}/webhook".format(NODE_IP)
        r = requests.get(url)
        print(url, r.status_code)
        if r.status_code != 200:
            send_message(RED, "NodeJS {}".format(action.capitalize()), "Alert {}".format(action.capitalize()), "{} fail with response code {}\nerror: {}".format(action.capitalize(), r.status_code, r.text))
            return
        return
    
    if len(words) < 3:
        send_message(RED, COMMAND_UNAVAILABLE, "", "Command `{}` is not available\n{}".format(message['text'], DEFAULT_CHAT_NODE_SERVER))
        return

    if not words[2].isdigit():
        send_message(RED, COMMAND_UNAVAILABLE, "", "Cannot use `{}` as upscale count\n{}".format(words[2], DEFAULT_CHAT_NODE_SERVER))
        return

    count = int(words[2])
    print("action: {}\ncount: {}".format(action, count))
    if action != "upscale" and action != "downscale":
        send_message(RED, COMMAND_UNAVAILABLE, "", "Action `{}` is not available\n{}".format(action, DEFAULT_CHAT_NODE_SERVER))
        return

    url = "{}/webhook/{}/{}".format(NODE_IP, action, count)
    r = requests.get(url)
    print(url, r.status_code)
    if r.status_code != 200:
        send_message(RED, "NodeJS {}".format(action.capitalize()), "Alert {}".format(action.capitalize()), "{} fail with response code {}\nerror: {}".format(action.capitalize(), r.status_code, r.text))
        return

@app.message("server_mongo")
def mongo_server(message, say):
    words = message['text'].split(' ')
    if len(words) != 2:
        send_message(RED, COMMAND_UNAVAILABLE, "", "Command `{}` is not available\n{}".format(message['text'], DEFAULT_CHAT_MONGO_SERVER))
        return
    action = words[1]
    print("action: {}".format(action))
    if action != "restart":
        send_message(RED, COMMAND_UNAVAILABLE, "", "Action `{}` is not available\n{}".format(action, DEFAULT_CHAT_MONGO_SERVER))
        return
    url = "{}/{}".format(MONGO_IP, action)
    print(url)
    r = requests.get(url)
    if r.status_code != 200:
        send_message(RED, "MongoDB {}".format(action.capitalize()), "Alert {}".format(action.capitalize()), "{} fail with response code {}\nerror: {}".format(action.capitalize(), r.status_code, r.text))

@app.message("help")
def help(message, say):
    send_message(GREEN, "Command", "Available Command", HELP)

@app.event("message")
def handle_message_events(body, logger):
    logger.info(body)

# Start your app
if __name__ == "__main__":
    app.start(port=int(os.environ.get("PORT", 3000)))