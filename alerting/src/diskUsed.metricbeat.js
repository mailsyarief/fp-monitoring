const fetch = require('node-fetch');
const {sendMessage} = require('./sendMessage.slack')
const LIMIT = 15
require('dotenv').config()

let messageData = {
    color: "#E0215A",
    origin: "Host Storage Warning",
    topic: "Storage used Warning",
    message: `Waspada! Penggunaan Storage meningkat!`,
    rate: "",
    limit: `${LIMIT}% storage used`
}

let query = JSON.stringify({
    "query": {
      "exists" : { "field" : "system.fsstat.total_size" }
    },
    "size": 0, 
    "aggs": {
      "by_hostname": {
        "terms": {
          "field": "host.name",
          "size": 100
        },
        "aggs": {
          "newst_fsstat": {
            "top_hits": {
              "sort": [{"@timestamp": {"order": "desc"}}],
              "size" : 1
            }
          },
          "total_size": {
            "sum": {
              "field": "system.fsstat.total_size.total"
            }
          },
          "used_size": {
            "sum": {
              "field": "system.fsstat.total_size.used"
            }
          },
          "disk_usage_percentage": {
            "bucket_script": {
              "buckets_path": {
                "total": "total_size",
                "used": "used_size"
              },
              "script": "params.used / params.total * 100"
            }
          }
        }
      }
    }
  })

module.exports = {
    storageAlert: () => {
        fetch(`${process.env.ELASTIC_HOST}/metricbeat-7.12.1/_search`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: query
        })
        .then(response => response.json())
        .then(result => {
            let diskUsageByHost = result.aggregations.by_hostname.buckets
            diskUsageByHost.map(data => {
                let host = data.key
                let value = (data.disk_usage_percentage.value).toFixed(1)
                if(value >= LIMIT){
                    messageData.rate = `${host} ${value}%`
                    sendMessage(messageData)
                    console.log(`storage alert from ${host} : ${value}`)
                }
            })
        });
    },
}
