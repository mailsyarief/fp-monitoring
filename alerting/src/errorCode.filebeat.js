const fetch = require('node-fetch');
const {sendMessage} = require('./sendMessage.slack')

require('dotenv').config()

let lastValue = 0
let requestRate = 1

let messageData = {
    color: "#E0215A",
    origin: "Nginx Filebeat",
    topic: "Alert Error Code",
    message: `Waspada! Jumlah Error Code meningkat!`,
    rate: "",
    limit: requestRate + " Error Code/10 Detik"
}
let query = JSON.stringify({ "aggs": { "2": { "date_range": { "field": "@timestamp", "ranges": [ { "from": "now-10s" } ], "time_zone": "Asia/Jakarta" } } }, "size": 0, "fields": [ { "field": "@timestamp", "format": "date_time" } ], "script_fields": {}, "stored_fields": [ "*" ], "runtime_mappings": {}, "_source": { "excludes": [] }, "query": { "bool": { "must": [], "filter": [ { "match_all": {} }, { "range": { "http.response.status_code": { "gte": 500, "lt": 599 } } }, { "range": { "@timestamp": { "gte": "now-10s" } } } ], "should": [], "must_not": [] } }})

module.exports = {
    errorCodeAlert: () => {
        fetch(`${process.env.ELASTIC_HOST}/filebeat-*/_search`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: query
        }).then(response => response.json())
            .then(data => {
                requestRate = data.aggregations['2'].buckets[0].doc_count
                if (requestRate > lastValue) {
                    messageData.rate = requestRate + " Error Code/10 Detik"
                    sendMessage(messageData)
                    lastValue = requestRate
                    console.log("Last Value 2 : ", lastValue)
                } else {
                    // console.log(requestRate)
                }
            });
    },
}

