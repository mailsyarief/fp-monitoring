const request = require('request');

require('dotenv').config()

module.exports = {
    sendMessage: (message, row=2) => {
        const headers = {
            'Content-type': 'application/json'
        };
        const attch = {
            "color": message.color,
            "pretext": "--------------",
            "author_name": message.origin,
            "author_icon": "http://flickr.com/icons/bobby.jpg",
            "title": message.topic,
            "title_link": "https://api.slack.com/",
            "text": message.message,
            "image_url": "http://my-website.com/path/to/image.jpg",
            "thumb_url": "http://example.com/path/to/thumb.png",
            "footer": "Kelompok Klasic Mantap!",
            "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
        }
        const fields = [
            {
                "title": "LIMIT : " + message.limit,
                "short": false
            },
            {
                "title": "CURRENT : " + message.rate,
                "short": false
            }
        ]
        const field = [
            {
                "title": message.text,
                "short": false
            }
        ]
        attch.fields = (row==2)?fields : field
        const messageObj = {
            "attachments": [ attch ]
        }
        const dataString = JSON.stringify(messageObj)
        const options = {
            url: process.env.WEBHOOK_URL,
            method: 'POST',
            headers: headers,
            body: dataString
        };

        function callback(error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body);
            } else {
                console.log(error)
            }
        }

        request(options, callback);
    }
}