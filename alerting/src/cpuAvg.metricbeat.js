const fetch = require('node-fetch');
const {sendMessage} = require('./sendMessage.slack')
const LIMIT = 80
require('dotenv').config()

let messageData = {
    color: "#E0215A",
    origin: "Host CPU Warning",
    topic: "CPU Usage Warning",
    message: `Waspada! Penggunaan CPU meningkat!`,
    rate: "",
    limit: `${LIMIT}% CPU Usage`
}

let query = JSON.stringify({ "aggs": { "2": { "terms": { "field": "host.name", "order": { "1": "desc" }, "size": 5 }, "aggs": { "1": { "avg": { "field": "system.cpu.user.pct" } } } } }, "size": 0, "fields": [ { "field": "@timestamp", "format": "date_time" } ], "script_fields": {}, "stored_fields": [ "*" ], "runtime_mappings": {}, "_source": { "excludes": [] }, "query": { "bool": { "must": [], "filter": [ { "match_all": {} }, { "range": { "@timestamp": { "gte": "now-30s" } } } ], "should": [], "must_not": [] } }})
module.exports = {
    cpuAvgAlert: () => {
        fetch(`${process.env.ELASTIC_HOST}/metricbeat-*/_search`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: query
        })
        .then(response => response.json())
        .then(data => {
            let cpuRate = data.aggregations['2'].buckets
            cpuRate.map((rate) => {
                let { value } = rate['1']
                let host = rate['key']
                value = (value * 100).toFixed(1)
                if(value >= LIMIT){
                    messageData.rate = `${host} ${value}%`
                    sendMessage(messageData)
                    console.log(`cpu alert from ${host} : ${value}`)
                }    
            })
        });
    },
}
