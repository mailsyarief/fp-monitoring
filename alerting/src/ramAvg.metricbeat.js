
const fetch = require('node-fetch');
const {sendMessage} = require('./sendMessage.slack')
const LIMIT = 75
require('dotenv').config()

let messageData = {
    color: "#E0215A",
    origin: "Host Memory Warning",
    topic: "Memory Usage Warning",
    message: `Waspada! Penggunaan Memory meningkat!`,
    rate: "",
    limit: `${LIMIT}% Memory Usage`
}
let query = JSON.stringify({ "aggs": { "2": { "terms": { "field": "host.name", "order": { "1": "desc" }, "size": 5 }, "aggs": { "1": { "avg": { "field": "system.memory.actual.used.pct" } } } } }, "size": 0, "fields": [ { "field": "@timestamp", "format": "date_time" } ], "script_fields": {}, "stored_fields": [ "*" ], "runtime_mappings": {}, "_source": { "excludes": [] }, "query": { "bool": { "must": [], "filter": [ { "match_all": {} }, { "range": { "@timestamp": { "gte": "now-30s" } } } ], "should": [], "must_not": [] } }})
module.exports = {
    ramAvgAlert: () => {
        fetch(`${process.env.ELASTIC_HOST}/metricbeat-*/_search`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: query
        })
        .then(response => response.json())
        .then(data => {
            let memoryRate = data.aggregations['2'].buckets
            memoryRate.map((rate) => {
                let { value } = rate['1']
                let host = rate['key']
                value = (value*100).toFixed(1)
                if(value >= LIMIT){
                    top = value
                    messageData.rate = `${host} ${value}%`
                    sendMessage(messageData)
                    console.log(`memory alert from ${host} : ${value}`)
                }        
            })
        });
    },
}
