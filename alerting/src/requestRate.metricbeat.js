const fetch = require('node-fetch');
const {sendMessage} = require('./sendMessage.slack')
const LIMIT = 15

require('dotenv').config()

let messageData = {
    color: "#af63ed",
    origin: "Nginx Metricbeat",
    topic: "Alert Request Rate",
    message: `Waspada! Jumlah request meningkat!`,
    rate: "",
    limit: LIMIT + " Request/30 Detik"
}
let query = JSON.stringify({"aggs":{"2":{"date_histogram":{"field":"@timestamp","fixed_interval":"10s","time_zone":"Asia\/Jakarta"},"aggs":{"1":{"derivative":{"buckets_path":"1-metric"}},"1-metric":{"avg":{"field":"nginx.stubstatus.requests"}}}}},"size":0,"fields":[{"field":"@timestamp","format":"date_time"}],"script_fields":{},"stored_fields":["*"],"runtime_mappings":{},"_source":{"excludes":[]},"query":{"bool":{"must":[],"filter":[{"match_all":{}},{"range":{"@timestamp":{"gte":"now-30s"}}}],"should":[],"must_not":[]}}})

module.exports = {
    requestRateAlert: () => {
        fetch(`${process.env.ELASTIC_HOST}/metricbeat-7.12.1/_search`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: query
        }).then(response => response.json())
            .then(data => {
                let requestRate = data.aggregations['2'].buckets[1]['1'].value
                if (requestRate > LIMIT) {
                    messageData.rate = requestRate + " Request/30 Detik"
                    sendMessage(messageData)
                    console.log(`requestRate alert ${requestRate}`)
                } 
            });
    },
}

