const fetch = require('node-fetch');
const {sendMessage} = require('./sendMessage.slack')

require('dotenv').config()

module.exports = {
    deployAlert: (color, message, text) => {
        let messageData = {
            color,
            origin: "Ansible Webhook",
            topic: "Alert Deploy I",
            message,
            rate: "",
            limit: "",
            text
        }
        sendMessage(messageData, 1)
    },
}

