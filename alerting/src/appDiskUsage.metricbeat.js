const fetch = require('node-fetch');
const {sendMessage} = require('./sendMessage.slack')
const LIMIT = 50
require('dotenv').config()

let messageData = {
    color: "#E0215A",
    origin: "APP Disk Usage Warning",
    // topic: "APP disk usage Warning",
    message: `Waspada! Disk usage pada APP meningkat!`,
    rate: "",
    limit: `${LIMIT}% disk usage`
}

let query = JSON.stringify({
    "aggs": {
      "2": {
        "terms": {
          "field": "system.filesystem.mount_point",
          "order": {
            "_key": "desc"
          },
          "size": 5
        },
        "aggs": {
          "1": {
            "top_hits": {
              "docvalue_fields": [
                {
                  "field": "system.filesystem.used.pct"
                }
              ],
              "_source": "system.filesystem.used.pct",
              "size": 1,
              "sort": [
                {
                  "@timestamp": {
                    "order": "desc"
                  }
                }
              ]
            }
          }
        }
      }
    },
    "size": 0,
    "fields": [
      {
        "field": "@timestamp",
        "format": "date_time"
      }
    ],
    "script_fields": {},
    "stored_fields": [
      "*"
    ],
    "runtime_mappings": {},
    "_source": {
      "excludes": []
    },
    "query": {
      "bool": {
        "must": [],
        "filter": [
          {
            "match_all": {}
          },
          {
            "match_phrase": {
            //   "host.name": "nut.local" 
              "host.name": "MAC10RP3XYCHIP.local"
            }
          },
          {
            "range": {
              "@timestamp": {
                "gte": "now-30s"
              }
            }
          }
        ],
        "should": [],
        "must_not": []
      }
    }
  })

module.exports = {
    appDiskAlert: () => {
        fetch(`${process.env.ELASTIC_HOST}/metricbeat-7.12.1/_search`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: query
        })
        .then(response => response.json())
        .then(result => {
            let { buckets } = result.aggregations['2']
            let sum = 0;
            buckets.map(bucket => {
                sum += bucket['1'].hits.hits[0].fields['system.filesystem.used.pct'][0]
            })
            sum = (sum*100).toFixed(1)
            console.log(sum)
            if(sum > LIMIT) {
                messageData.rate = `${sum}% disk usage`
                console.log(`APP disk usage alert : ${sum}`)
                sendMessage(messageData)
            }
        });
    },
}
