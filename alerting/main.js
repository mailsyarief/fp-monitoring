const schedule = require('node-schedule')

const express = require('express')
const app = express()
const port = 3004

app.get('/', (req, res) => {
    res.json({ message: `Alert API` });
})

const { requestRateAlert } = require("./src/requestRate.metricbeat")
const { errorCodeAlert } = require("./src/errorCode.filebeat")
const { deployAlert } = require("./src/deployComplete.docker")
const { cpuAvgAlert } = require("./src/cpuAvg.metricbeat")
const { ramAvgAlert } = require("./src/ramAvg.metricbeat")
const { storageAlert } = require("./src/diskUsed.metricbeat")
const { dbDiskAlert } = require("./src/dbDiskUsage.metricbeat")
const { appDiskAlert } = require("./src/appDiskUsage.metricbeat")


function main() {
    schedule.scheduleJob('*/10 * * * * *', function () {
        requestRateAlert()
        errorCodeAlert()
        cpuAvgAlert()
        ramAvgAlert()   
        storageAlert()
        dbDiskAlert()
        appDiskAlert()
    });

    app.post('/deploy/:color/:message/:text', (req, res) => {
        const red = '#ff006e'
        const green = '#ADFF2F'
        const color = req.params.color == 'red'? red : green;
        const {message, text} = req.params
        deployAlert(color, message, text)
        res.sendStatus(200)
    })

    app.listen(port, () => {
        console.log(`Example app listening at http://localhost:${port}`)
    })
}


main()