const { exec } = require("child_process");
const fetch = require("node-fetch");
const express = require('express')
const app = express()
const port = 3005
const alert_ip = 'localhost:3004'
const headers = {
    "Content-type": "application/x-www-form-urlencoded",
  }
const triggerDeployAlert = (color, message, text) => {
    fetch(`http://${alert_ip}/deploy/${color}/${message}/${text}`, {
        headers,
        method: "POST",
    }).then(res => console.log(res))
}

app.get('/restart', (req, res) => {
    //execute ansible playbook here
    const startTime = new Date()
    exec("ansible-playbook -i inventory.yaml playbook.yaml --connection=local", (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            triggerDeployAlert('red', 'Restart MongoDB Gagal!', error.message)
            return res.json({ message: `error: ${error.message}` });
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            triggerDeployAlert('red', 'Restart MongoDB Gagal!', stderr)           
            return res.json({ message: `stderr: ${stderr}` });
        }
        const finishTime = new Date()
        console.log(startTime.getTime(), finishTime.getTime());
        const time = (finishTime - startTime)/1000
        const seconds = Math.abs(Math.round(time))
        console.log(`stdout: ${stdout}`);
        triggerDeployAlert('green', 'Restart MongoDB Berhasil!', `Restart duration : ${seconds} seconds`)
        return res.json({ message: `stdout: ${stdout}` });
    });
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
  })
