import fileinput
import sys

def replace(file, searchExp, replaceExp):
   for line in fileinput.input(file, inplace=1):
       line = line.replace(searchExp, replaceExp)
       sys.stdout.write(line)

def main():
    option = sys.argv[1]
    argument = sys.argv[2]
    print(option)
    print(argument)

    if option == 'disable':
        disable(argument)
    
    if option == 'enable':
        enable(argument)

def enable(args):
    replace("./nginx/default.conf", "#server {}:3000".format(args), "server {}:3000".format(args))

def disable(args):
    replace("./nginx/default.conf", "server {}:3000".format(args), "#server {}:3000".format(args))

main()