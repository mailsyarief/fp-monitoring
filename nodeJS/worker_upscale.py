import yaml
import sys
import os
import time

FILENAME = "docker-compose.yml"
NEW_NODE = {
    'command': 'npm start',
    'environment': ['NODE_ENV=production'],
    'image': 'node:10.14',
    'user': 'node',
    'volumes': ['./:/home/node/app'],
    'working_dir': '/home/node/app'
}
VERSION = {
    'version': '2.2'
}
NETWORKS = {
    'networks': {'default': {'external': {'name': 'external'}}}
}
for x in range(int(sys.argv[1])):
    with open(FILENAME, "r") as yamlfile:
        cur_yaml = yaml.safe_load(yamlfile) # Note the safe_load
        current_node = (len(cur_yaml['services']))
        node_name = "node%02d" % int(current_node)
        new_node = {node_name: NEW_NODE}
        cur_yaml['services'].update(new_node)
    if cur_yaml:
        with open(FILENAME, 'w') as yamlfile:
            yaml.safe_dump({'version' :cur_yaml['version']}, yamlfile)
            yaml.safe_dump({'services':cur_yaml['services']}, yamlfile)
            yaml.safe_dump({'networks':cur_yaml['networks']}, yamlfile)
    os.system('python3 ./update_conf.py enable ' + node_name)
    os.system('docker-compose up -d')

time.sleep(2 * int(sys.argv[1]))
os.system('sh check.sh ' + node_name)
os.system('docker exec nodejs_nginx_1 nginx -s reload')
    

