const { exec } = require("child_process");
const fetch = require("node-fetch");
const express = require('express')
const app = express()
const port = 3003
const alert_ip = 'localhost:3004'
const headers = {
  "Content-type": "application/x-www-form-urlencoded",
}
const triggerDeployAlert = (color, message, text) => {
  console.log(typeof text)
  fetch(`http://${alert_ip}/deploy/${color}/${message}/${text}`, {
      headers,
      method: "POST",
  }).then(res => console.log(res))
}

app.get('/', (req, res) => {
    res.json({ message: `Ansible Webhook` });
})

app.get('/webhook', (req, res) => {
  const startTime = new Date()
    exec("ansible-playbook -i inventory.yaml playbook.yaml --connection=local", (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            triggerDeployAlert('red', 'Deploy Gagal!', error.message)
            return res.json({ message: `error: ${error.message}` });
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            triggerDeployAlert('red', 'Deploy Gagal!', stderr)           
            return res.json({ message: `stderr: ${stderr}` });
        }
        const finishTime = new Date()
        console.log(startTime.getTime(), finishTime.getTime());
        const time = (finishTime - startTime)/1000
        const seconds = Math.abs(Math.round(time))
        console.log(`stdout: ${stdout}`);
        triggerDeployAlert('green', 'Deploy Berhasil!', `Deploy duration : ${seconds} seconds`)
        return res.json({ message: `stdout: ${stdout}` });
    });
})

app.get('/webhook/upscale/:count', (req, res) => {
  const startTime = new Date()
    exec("python3 worker_upscale.py " + req.params.count, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            triggerDeployAlert('red', 'Upscale NodeJS Gagal!', error.message)
            return res.json({ message: `error: ${error.message}` });
        }
        const finishTime = new Date()
        console.log(startTime.getTime(), finishTime.getTime());
        const time = (finishTime - startTime)/1000
        const seconds = Math.abs(Math.round(time))
        console.log(`stdout: ${stdout}`);
        triggerDeployAlert('green', 'Upscale NodeJS Berhasil!', `Upscale duration : ${seconds} seconds\nUpscale Count ${req.params.count}`)
        return res.json({ message: `stdout: ${stdout}` });
    });
})

app.get('/webhook/downscale/:count', (req, res) => {
  const startTime = new Date()
    exec("python3 worker_downscale.py " + req.params.count, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            triggerDeployAlert('red', 'Downscale NodeJS Gagal!', error.message)
            return res.json({ message: `error: ${error.message}` });
        }
        const finishTime = new Date()
        console.log(startTime.getTime(), finishTime.getTime());
        const time = (finishTime - startTime)/1000
        const seconds = Math.abs(Math.round(time))
        console.log(`stdout: ${stdout}`);
        triggerDeployAlert('green', 'Downscale NodeJS Berhasil!', `Downscale duration : ${seconds} seconds\nDownscale Count ${req.params.count}`)
        return res.json({ message: `stdout: ${stdout}` });
    });
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})