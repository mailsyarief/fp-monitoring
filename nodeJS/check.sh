# docker run node:10.14
# echo $1
node=$1
echo node
docker run --rm --network=external -v $(pwd)/:/root/app node:10.14 /bin/bash -c "cd /root/app/ && NODE_ENV=${node} node readyness.js"