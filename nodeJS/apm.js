let apmState

module.exports = {
    start: () => {
        const apm = require('elastic-apm-node')
        apm.start({
            // Override the service name from package.json
            // Allowed characters: a-z, A-Z, 0-9, -, _, and space
            serviceName: 'project-crud',
    
            // Use if APM Server requires a secret token
            secretToken: '',
    
            // Set the custom APM Server URL (default: http://localhost:8200)
            serverUrl: 'http://docker.for.mac.host.internal:8200',
    
            // Set the service environment
            environment: 'production'
        })
        apmState = apm
    },

    error: (message) => {
        console.log("error to apm", message)
        apmState.captureError(message)
    }
    
}