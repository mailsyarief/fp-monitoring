import yaml
import sys
import os

FILENAME = "docker-compose.yml"
VERSION = {
    'version': '2.2'
}
NETWORKS = {
    'networks': {'default': {'external': {'name': 'external'}}}
}

for x in range(int(sys.argv[1])):
    with open(FILENAME, "r") as yamlfile:
        cur_yaml = yaml.safe_load(yamlfile) # Note the safe_load
        current_node = (len(cur_yaml['services']))
        node_name = "node%02d" % int(current_node - 1)
        cur_yaml['services'].pop(node_name)
    if cur_yaml:
        with open(FILENAME, 'w') as yamlfile:
            yaml.safe_dump({'version' :cur_yaml['version']}, yamlfile)
            yaml.safe_dump({'services':cur_yaml['services']}, yamlfile)
            yaml.safe_dump({'networks':cur_yaml['networks']}, yamlfile)
    
    os.system('python3 ./update_conf.py disable ' + node_name)
    
os.system('docker exec nodejs_nginx_1 nginx -s reload')
os.system('docker-compose up -d --remove-orphans')

