# FP Kelompok Klasic

Klasic = Kelompok Elastic,Jalankan secara berurutan :)

## 1. Network Installation


```bash
docker network create external
```

## 2. DB Installation


```bash
cd mongoDB
docker-compose up -d
```

## 3. App Installation
```bash
cd nodeJS
npm update
npm install
sh start.sh
```

## 4. Alert Installation
```bash
cd alerting
npm update
npm install
node main.js
```

## 4. Spam Request
```bash
cd spamRequest
npm update
npm install
node insert.js
```
