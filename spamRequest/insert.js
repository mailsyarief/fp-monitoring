const fetch = require('node-fetch');
const querystring = require('querystring');

const ip = '10.6.7.243'
const headers = {
    "Content-type": "application/x-www-form-urlencoded",
}
const employeeData = {
    fullName: "test data",
    email: "mail@mail.com",
    mobile: "0123456789",
    city: "jakarta"
}

const listData = () => {
    fetch(`http://${ip}:8080/employee/list`, {
        headers,
        method: "GET",
    })
    .then(res => {
        if(res.status == 200) console.log("Get list endpoint");
        else console.log("failed Get list endpoint");
    })
}

const latencyData = () => {
    fetch(`http://${ip}:8080/employee/latency`, {
        headers,
        method: "GET",
    })
    .then(res => {
        if(res.status == 200) console.log("Get latency endpoint");
        else console.log("failed Get latency endpoint");
    })
}


const notFoundData = () => {
    fetch(`http://${ip}:8080/22`, {
        headers,
        method: "GET",
    })
    .then(res => {
        if(res.status == 200) console.log("Get latency endpoint");
        else console.log("failed Get latency endpoint");
    })
}

const exampleData = () => {
    fetch(`http://${ip}:8080/employee/example`, {
        headers,
        method: "GET",
    })
    .then(res => {
        if(res.status == 200) console.log("Get example endpoint");
        else console.log("failed Get example endpoint");
    })
}

const insertData = () => {
    fetch(`http://${ip}:8080/newEmployee`, {
        headers,
        method: "POST",
        body: querystring.stringify(employeeData)
    })
    .then(res => {
        if(res.status == 201) console.log("successfully add new employee data");
        else console.log("failed to add new employee data");
    })
}

const deleteData = () => {
    fetch(`http://${ip}:8080/deleteEmployee`, {
        headers,
        method: "DELETE"
    })
    .then(res => {
        if(res.status == 200) console.log("successfully delete employee data");
        else console.log("failed to delete employee data");
    })
}

const getInternalServerError = () => {
    fetch(`http://${ip}:8080/getError`, {
        headers,
        method: "POST",
        body: querystring.stringify(employeeData)
    })
    .then(res => {
        if(res.status != 500) console.log("failed at get Internal Server Error")
        else console.log("Internal Server Error")
    })
}

const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const spammingRequest = async(time) => {
    while(true) {
        insertData()
        await sleep(time);
        deleteData()
        await sleep(time);
        listData()
        await sleep(time);
        exampleData()
        await sleep(time);
        latencyData()
        notFoundData()
        await sleep(time);
        getInternalServerError()
    } 
}
let argv = process.argv.slice(2)[0]
spammingRequest(argv);


